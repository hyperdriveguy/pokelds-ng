INCLUDE "constants.asm"


SECTION "Audio", ROMX

INCLUDE "audio/engine.asm"
INCLUDE "data/trainers/encounter_music.asm"
INCLUDE "audio/music_pointers.asm"
INCLUDE "audio/music/gsc/nothing.asm"
INCLUDE "audio/cry_pointers.asm"
INCLUDE "audio/sfx_pointers.asm"



SECTION "GS Songs 1", ROMX

INCLUDE "audio/music/gsc/route36.asm"
INCLUDE "audio/music/gsc/rivalbattle.asm"
INCLUDE "audio/music/gsc/rocketbattle.asm"
INCLUDE "audio/music/gsc/elmslab.asm"
INCLUDE "audio/music/gsc/darkcave.asm"
INCLUDE "audio/music/gsc/johtogymbattle.asm"
INCLUDE "audio/music/gsc/championbattle.asm"
INCLUDE "audio/music/gsc/ssaqua.asm"
INCLUDE "audio/music/gsc/newbarktown.asm"
INCLUDE "audio/music/gsc/goldenrodcity.asm"
INCLUDE "audio/music/gsc/vermilioncity.asm"
INCLUDE "audio/music/gsc/titlescreen.asm"
INCLUDE "audio/music/gsc/ruinsofalphinterior.asm"
INCLUDE "audio/music/gsc/lookpokemaniac.asm"
INCLUDE "audio/music/gsc/trainervictory.asm"


SECTION "GS Songs 2", ROMX

INCLUDE "audio/music/gsc/route1.asm"
INCLUDE "audio/music/gsc/route3.asm"
INCLUDE "audio/music/gsc/route12.asm"
INCLUDE "audio/music/gsc/kantogymbattle.asm"
INCLUDE "audio/music/gsc/kantotrainerbattle.asm"
INCLUDE "audio/music/gsc/kantowildbattle.asm"
INCLUDE "audio/music/gsc/pokemoncenter.asm"
INCLUDE "audio/music/gsc/looklass.asm"
INCLUDE "audio/music/gsc/lookofficer.asm"
INCLUDE "audio/music/gsc/route2.asm"
INCLUDE "audio/music/gsc/mtmoon.asm"
INCLUDE "audio/music/gsc/showmearound.asm"
INCLUDE "audio/music/gsc/gamecorner.asm"
INCLUDE "audio/music/gsc/bicycle.asm"
INCLUDE "audio/music/gsc/looksage.asm"
INCLUDE "audio/music/gsc/pokemonchannel.asm"
INCLUDE "audio/music/gsc/lighthouse.asm"
INCLUDE "audio/music/gsc/lakeofrage.asm"
INCLUDE "audio/music/gsc/indigoplateau.asm"
INCLUDE "audio/music/gsc/route37.asm"
INCLUDE "audio/music/gsc/rockethideout.asm"
INCLUDE "audio/music/gsc/dragonsden.asm"
INCLUDE "audio/music/gsc/ruinsofalphradio.asm"
INCLUDE "audio/music/gsc/lookbeauty.asm"
INCLUDE "audio/music/gsc/route26.asm"
INCLUDE "audio/music/gsc/ecruteakcity.asm"
INCLUDE "audio/music/gsc/lakeofragerocketradio.asm"
INCLUDE "audio/music/gsc/magnettrain.asm"
INCLUDE "audio/music/gsc/lavendertown.asm"
INCLUDE "audio/music/gsc/dancinghall.asm"
INCLUDE "audio/music/gsc/contestresults.asm"
INCLUDE "audio/music/gsc/route30.asm"


SECTION "GS Songs 3", ROMX

INCLUDE "audio/music/gsc/violetcity.asm"
INCLUDE "audio/music/gsc/route29.asm"
INCLUDE "audio/music/gsc/halloffame.asm"
INCLUDE "audio/music/gsc/healpokemon.asm"
INCLUDE "audio/music/gsc/evolution.asm"
INCLUDE "audio/music/gsc/printer.asm"


SECTION "GS Songs 4", ROMX

INCLUDE "audio/music/gsc/viridiancity.asm"
INCLUDE "audio/music/gsc/celadoncity.asm"
INCLUDE "audio/music/gsc/wildpokemonvictory.asm"
INCLUDE "audio/music/gsc/successfulcapture.asm"
INCLUDE "audio/music/gsc/gymleadervictory.asm"
INCLUDE "audio/music/gsc/mtmoonsquare.asm"
INCLUDE "audio/music/gsc/gym.asm"
INCLUDE "audio/music/gsc/pallettown.asm"
INCLUDE "audio/music/gsc/profoakspokemontalk.asm"
INCLUDE "audio/music/gsc/profoak.asm"
INCLUDE "audio/music/gsc/lookrival.asm"
INCLUDE "audio/music/gsc/aftertherivalfight.asm"
INCLUDE "audio/music/gsc/surf.asm"
INCLUDE "audio/music/gsc/nationalpark.asm"
INCLUDE "audio/music/gsc/azaleatown.asm"
INCLUDE "audio/music/gsc/cherrygrovecity.asm"
INCLUDE "audio/music/gsc/unioncave.asm"
INCLUDE "audio/music/gsc/johtowildbattle.asm"
INCLUDE "audio/music/gsc/johtowildbattlenight.asm"
INCLUDE "audio/music/gsc/johtotrainerbattle.asm"
INCLUDE "audio/music/gsc/lookyoungster.asm"
INCLUDE "audio/music/gsc/tintower.asm"
INCLUDE "audio/music/gsc/sprouttower.asm"
INCLUDE "audio/music/gsc/burnedtower.asm"
INCLUDE "audio/music/gsc/mom.asm"
INCLUDE "audio/music/gsc/victoryroad.asm"
INCLUDE "audio/music/gsc/pokemonlullaby.asm"
INCLUDE "audio/music/gsc/pokemonmarch.asm"
INCLUDE "audio/music/gsc/goldsilveropening.asm"
INCLUDE "audio/music/gsc/goldsilveropening2.asm"
INCLUDE "audio/music/gsc/lookhiker.asm"
INCLUDE "audio/music/gsc/lookrocket.asm"
INCLUDE "audio/music/gsc/rockettheme.asm"
INCLUDE "audio/music/gsc/mainmenu.asm"
INCLUDE "audio/music/gsc/lookkimonogirl.asm"
INCLUDE "audio/music/gsc/pokeflutechannel.asm"
INCLUDE "audio/music/gsc/bugcatchingcontest.asm"


SECTION "Crystal Songs 1", ROMX

INCLUDE "audio/music/gsc/mobileadaptermenu.asm"
INCLUDE "audio/music/gsc/buenaspassword.asm"
INCLUDE "audio/music/gsc/lookmysticalman.asm"
INCLUDE "audio/music/gsc/crystalopening.asm"
INCLUDE "audio/music/gsc/battletowertheme.asm"
INCLUDE "audio/music/gsc/suicunebattle.asm"
INCLUDE "audio/music/gsc/battletowerlobby.asm"
INCLUDE "audio/music/gsc/mobilecenter.asm"



SECTION "Crystal Songs 2", ROMX

INCLUDE "audio/music/gsc/credits.asm"
INCLUDE "audio/music/gsc/clair.asm"
INCLUDE "audio/music/gsc/mobileadapter.asm"
INCLUDE "audio/music/gsc/postcredits.asm"



SECTION "Pokemon LDS Songs", ROMX

INCLUDE "audio/music/lds/bookofmormonstories.asm"
INCLUDE "audio/music/lds/bringhistruth.asm"
INCLUDE "audio/music/lds/sevennation.asm"
INCLUDE "audio/music/lds/missionarybattle.asm"


SECTION "Sound Effects", ROMX

INCLUDE "audio/sfx.asm"


SECTION "Crystal Sound Effects", ROMX

INCLUDE "audio/sfx_crystal.asm"


SECTION "Cries", ROMX

INCLUDE "data/pokemon/cries.asm"

INCLUDE "audio/cries.asm"
