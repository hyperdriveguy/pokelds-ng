# Pokémon LDS (Next Generation)

A game heavily inspired by Pokémon and LDS Culture.

To set up the repository, see [INSTALL.md](INSTALL.md).

Join the [Discord Server](https://discord.gg/TsaRSes)

_Not affiliated with or endorsed by Nintendo or the Church of Jesus Christ of Latter-day Saints._
