ItemPocketNames:
; entries correspond to item type constants
	dw .Item
	dw .Key
	dw .Ball
	dw .TM
	dw .Food
	dw .Med
	dw .Bat

.Item: db "ITEM POCKET@"
.Key:  db "KEY POCKET@"
.Ball: db "BALL POCKET@"
.TM:   db "TM POCKET@"
.Food: db "FOOD POCKET@"
.Med:  db "MED POCKET@"
.Bat:  db "BATTLE POCKET@"
