SECTION "Map Blocks 1", ROMX

Route32_Blocks:
	INCBIN "maps/gsc/Route32.blk"

Route40_Blocks:
	INCBIN "maps/gsc/Route40.blk"

Route36_Blocks:
	INCBIN "maps/gsc/Route36.blk"

Route44_Blocks:
	INCBIN "maps/gsc/Route44.blk"

Route28_Blocks:
	INCBIN "maps/gsc/Route28.blk"

CeladonCity_Blocks:
	INCBIN "maps/gsc/CeladonCity.blk"

SaffronCity_Blocks:
	INCBIN "maps/gsc/SaffronCity.blk"

Route2_Blocks:
	INCBIN "maps/gsc/Route2.blk"

ElmsHouse_Blocks:
	INCBIN "maps/gsc/ElmsHouse.blk"

Route11_Blocks:
	INCBIN "maps/gsc/Route11.blk"

Route15_Blocks:
	INCBIN "maps/gsc/Route15.blk"

Route19_Blocks:
	INCBIN "maps/gsc/Route19.blk"

Route10South_Blocks:
	INCBIN "maps/gsc/Route10South.blk"

Pokecenter2F_Blocks:
CinnabarPokecenter2FBeta_Blocks:
CeruleanPokecenter2FBeta_Blocks:
Route10Pokecenter2FBeta_Blocks:
VermilionPokecenter2FBeta_Blocks:
PewterPokecenter2FBeta_Blocks:
FuchsiaPokecenter2FBeta_Blocks:
LavenderPokecenter2FBeta_Blocks:
CeladonPokecenter2FBeta_Blocks:
ViridianPokecenter2FBeta_Blocks:
SaffronPokecenter2FBeta_Blocks:
	INCBIN "maps/gsc/Pokecenter2F.blk"

Route41_Blocks:
	INCBIN "maps/gsc/Route41.blk"

Route33_Blocks:
	INCBIN "maps/gsc/Route33.blk"

Route45_Blocks:
	INCBIN "maps/gsc/Route45.blk"

Route29_Blocks:
	INCBIN "maps/gsc/Route29.blk"

Route37_Blocks:
	INCBIN "maps/gsc/Route37.blk"

LavenderTown_Blocks:
	INCBIN "maps/gsc/LavenderTown.blk"

PalletTown_Blocks:
	INCBIN "maps/gsc/PalletTown.blk"

Route25_Blocks:
	INCBIN "maps/gsc/Route25.blk"

Route24_Blocks:
	INCBIN "maps/gsc/Route24.blk"

Route3_Blocks:
	INCBIN "maps/gsc/Route3.blk"

PewterCity_Blocks:
	INCBIN "maps/gsc/PewterCity.blk"

Route12_Blocks:
	INCBIN "maps/gsc/Route12.blk"

Route20_Blocks:
	INCBIN "maps/gsc/Route20.blk"

Route30_Blocks:
	INCBIN "maps/gsc/Route30.blk"

Route26_Blocks:
	INCBIN "maps/gsc/Route26.blk"

Route42_Blocks:
	INCBIN "maps/gsc/Route42.blk"

Route34_Blocks:
	INCBIN "maps/gsc/Route34.blk"

Route46_Blocks:
	INCBIN "maps/gsc/Route46.blk"

FuchsiaCity_Blocks:
	INCBIN "maps/gsc/FuchsiaCity.blk"

Route38_Blocks:
	INCBIN "maps/gsc/Route38.blk"

OlivineTimsHouse_Blocks:
OlivineHouseBeta_Blocks:
OlivinePunishmentSpeechHouse_Blocks:
OlivineGoodRodHouse_Blocks:
Route39Farmhouse_Blocks:
MahoganyRedGyaradosSpeechHouse_Blocks:
BlackthornDragonSpeechHouse_Blocks:
BlackthornEmysHouse_Blocks:
MoveDeletersHouse_Blocks:
CeruleanGymBadgeSpeechHouse_Blocks:
CeruleanPoliceStation_Blocks:
CeruleanTradeSpeechHouse_Blocks:
BillsHouse_Blocks:
CharcoalKiln_Blocks:
LakeOfRageHiddenPowerHouse_Blocks:
LakeOfRageMagikarpHouse_Blocks:
GoldenrodHappinessRater_Blocks:
BillsFamilysHouse_Blocks:
GoldenrodPPSpeechHouse_Blocks:
GoldenrodNameRater_Blocks:
VermilionFishingSpeechHouse_Blocks:
VermilionMagnetTrainSpeechHouse_Blocks:
VermilionDiglettsCaveSpeechHouse_Blocks:
BluesHouse_Blocks:
PewterNidoranSpeechHouse_Blocks:
PewterSnoozeSpeechHouse_Blocks:
BillsBrothersHouse_Blocks:
LavenderSpeechHouse_Blocks:
LavenderNameRater_Blocks:
Route12SuperRodHouse_Blocks:
Route28SteelWingHouse_Blocks:
CeladonMansionRoofHouse_Blocks:
Route16FuchsiaSpeechHouse_Blocks:
ManiasHouse_Blocks:
CianwoodPharmacy_Blocks:
CianwoodPhotoStudio_Blocks:
CianwoodLugiaSpeechHouse_Blocks:
PokeSeersHouse_Blocks:
ViridianNicknameSpeechHouse_Blocks:
Route2NuggetHouse_Blocks:
PlayersNeighborsHouse_Blocks:
Route26HealHouse_Blocks:
DayOfWeekSiblingsHouse_Blocks:
Route27SandstormHouse_Blocks:
MrPsychicsHouse_Blocks:
Route5CleanseTagHouse_Blocks:
CherrygroveGymSpeechHouse_Blocks:
GuideGentsHouse_Blocks:
CherrygroveEvolutionSpeechHouse_Blocks:
Route30BerryHouse_Blocks:
	INCBIN "maps/gsc/House1.blk"

SafariZoneFuchsiaGateBeta_Blocks:
Route19FuchsiaGate_Blocks:
Route43MahoganyGate_Blocks:
Route43Gate_Blocks:
Route35GoldenrodGate_Blocks:
Route36RuinsOfAlphGate_Blocks:
Route34IlexForestGate_Blocks:
Route6SaffronGate_Blocks:
Route40BattleTowerGate_Blocks:
Route2Gate_Blocks:
Route29Route46Gate_Blocks:
Route5SaffronGate_Blocks:
	INCBIN "maps/gsc/NorthSouthGate.blk"

CinnabarIsland_Blocks:
	INCBIN "maps/gsc/CinnabarIsland.blk"

Route4_Blocks:
	INCBIN "maps/gsc/Route4.blk"

Route8_Blocks:
	INCBIN "maps/gsc/Route8.blk"

ViridianCity_Blocks:
	INCBIN "maps/gsc/ViridianCity.blk"

Route13_Blocks:
	INCBIN "maps/gsc/Route13.blk"

Route21_Blocks:
	INCBIN "maps/gsc/Route21.blk"

Route17_Blocks:
	INCBIN "maps/gsc/Route17.blk"

Route31_Blocks:
	INCBIN "maps/gsc/Route31.blk"

Route27_Blocks:
	INCBIN "maps/gsc/Route27.blk"

Route35_Blocks:
	INCBIN "maps/gsc/Route35.blk"

Route43_Blocks:
	INCBIN "maps/gsc/Route43.blk"

Route39_Blocks:
	INCBIN "maps/gsc/Route39.blk"

PlayersHouse1F_Blocks:
	INCBIN "maps/gsc/PlayersHouse1F.blk"

Route38EcruteakGate_Blocks:
Route42EcruteakGate_Blocks:
Route32RuinsOfAlphGate_Blocks:
IlexForestAzaleaGate_Blocks:
Route15FuchsiaGate_Blocks:
Route8SaffronGate_Blocks:
Route16Gate_Blocks:
Route7SaffronGate_Blocks:
Route17Route18Gate_Blocks:
Route31VioletGate_Blocks:
	INCBIN "maps/gsc/EastWestGate.blk"

VermilionCity_Blocks:
	INCBIN "maps/gsc/VermilionCity.blk"

ElmsLab_Blocks:
	INCBIN "maps/gsc/ElmsLab.blk"

CeruleanCity_Blocks:
	INCBIN "maps/gsc/CeruleanCity.blk"

Route1_Blocks:
	INCBIN "maps/gsc/Route1.blk"

Route5_Blocks:
	INCBIN "maps/gsc/Route5.blk"

Route9_Blocks:
	INCBIN "maps/gsc/Route9.blk"

Route22_Blocks:
	INCBIN "maps/gsc/Route22.blk"


SECTION "Map Blocks 2", ROMX

Route14_Blocks:
	INCBIN "maps/gsc/Route14.blk"

OlivineMart_Blocks:
EcruteakMart_Blocks:
BlackthornMart_Blocks:
CeruleanMart_Blocks:
AzaleaMart_Blocks:
VioletMart_Blocks:
VermilionMart_Blocks:
PewterMart_Blocks:
FuchsiaMart_Blocks:
LavenderMart_Blocks:
ViridianMart_Blocks:
SaffronMart_Blocks:
CherrygroveMart_Blocks:
	INCBIN "maps/gsc/Mart.blk"

Route10North_Blocks:
	INCBIN "maps/gsc/Route10North.blk"

OlivinePokecenter1F_Blocks:
MahoganyPokecenter1F_Blocks:
EcruteakPokecenter1F_Blocks:
BlackthornPokecenter1F_Blocks:
CinnabarPokecenter1F_Blocks:
CeruleanPokecenter1F_Blocks:
Route10Pokecenter1F_Blocks:
AzaleaPokecenter1F_Blocks:
VioletPokecenter1F_Blocks:
Route32Pokecenter1F_Blocks:
GoldenrodPokecenter1F_Blocks:
VermilionPokecenter1F_Blocks:
PewterPokecenter1F_Blocks:
FuchsiaPokecenter1F_Blocks:
LavenderPokecenter1F_Blocks:
SilverCavePokecenter1F_Blocks:
CeladonPokecenter1F_Blocks:
CianwoodPokecenter1F_Blocks:
ViridianPokecenter1F_Blocks:
SaffronPokecenter1F_Blocks:
CherrygrovePokecenter1F_Blocks:
	INCBIN "maps/gsc/Pokecenter1F.blk"

EarlsPokemonAcademy_Blocks:
	INCBIN "maps/gsc/EarlsPokemonAcademy.blk"

GoldenrodDeptStore1F_Blocks:
CeladonDeptStore1F_Blocks:
	INCBIN "maps/gsc/DeptStore1F.blk"

GoldenrodDeptStore2F_Blocks:
CeladonDeptStore2F_Blocks:
	INCBIN "maps/gsc/DeptStore2F.blk"

GoldenrodDeptStore3F_Blocks:
CeladonDeptStore3F_Blocks:
	INCBIN "maps/gsc/DeptStore3F.blk"

GoldenrodDeptStore4F_Blocks:
CeladonDeptStore4F_Blocks:
	INCBIN "maps/gsc/DeptStore4F.blk"

GoldenrodDeptStore5F_Blocks:
CeladonDeptStore5F_Blocks:
	INCBIN "maps/gsc/DeptStore5F.blk"

GoldenrodDeptStore6F_Blocks:
CeladonDeptStore6F_Blocks:
	INCBIN "maps/gsc/DeptStore6F.blk"

GoldenrodDeptStoreElevator_Blocks:
CeladonDeptStoreElevator_Blocks:
	INCBIN "maps/gsc/DeptStoreElevator.blk"

CeladonMansion1F_Blocks:
	INCBIN "maps/gsc/CeladonMansion1F.blk"

CeladonMansion2F_Blocks:
	INCBIN "maps/gsc/CeladonMansion2F.blk"

CeladonMansion3F_Blocks:
	INCBIN "maps/gsc/CeladonMansion3F.blk"

CeladonMansionRoof_Blocks:
	INCBIN "maps/gsc/CeladonMansionRoof.blk"

CeladonGameCorner_Blocks:
	INCBIN "maps/gsc/CeladonGameCorner.blk"

CeladonGameCornerPrizeRoom_Blocks:
	INCBIN "maps/gsc/CeladonGameCornerPrizeRoom.blk"

Colosseum_Blocks:
	INCBIN "maps/gsc/Colosseum.blk"

TradeCenter_Blocks:
TimeCapsule_Blocks:
	INCBIN "maps/gsc/TradeCenter.blk"

EcruteakLugiaSpeechHouse_Blocks:
EcruteakItemfinderHouse_Blocks:
VioletNicknameSpeechHouse_Blocks:
VioletKylesHouse_Blocks:
	INCBIN "maps/gsc/House2.blk"

UnionCaveB1F_Blocks:
	INCBIN "maps/gsc/UnionCaveB1F.blk"

UnionCaveB2F_Blocks:
	INCBIN "maps/gsc/UnionCaveB2F.blk"

UnionCave1F_Blocks:
	INCBIN "maps/gsc/UnionCave1F.blk"

NationalPark_Blocks:
NationalParkBugContest_Blocks:
	INCBIN "maps/gsc/NationalPark.blk"

Route5UndergroundPathEntrance_Blocks:
Route6UndergroundPathEntrance_Blocks:
	INCBIN "maps/gsc/UndergroundPathEntrance.blk"

KurtsHouse_Blocks:
	INCBIN "maps/gsc/KurtsHouse.blk"

GoldenrodMagnetTrainStation_Blocks:
	INCBIN "maps/gsc/GoldenrodMagnetTrainStation.blk"

RuinsOfAlphOutside_Blocks:
	INCBIN "maps/gsc/RuinsOfAlphOutside.blk"

RuinsOfAlphInnerChamber_Blocks:
	INCBIN "maps/gsc/RuinsOfAlphInnerChamber.blk"

RuinsOfAlphHoOhChamber_Blocks:
RuinsOfAlphKabutoChamber_Blocks:
RuinsOfAlphOmanyteChamber_Blocks:
RuinsOfAlphAerodactylChamber_Blocks:
	INCBIN "maps/gsc/RuinsOfAlphPuzzleChamber.blk"

SproutTower1F_Blocks:
	INCBIN "maps/gsc/SproutTower1F.blk"

SproutTower2F_Blocks:
	INCBIN "maps/gsc/SproutTower2F.blk"

SproutTower3F_Blocks:
	INCBIN "maps/gsc/SproutTower3F.blk"

RadioTower1F_Blocks:
	INCBIN "maps/gsc/RadioTower1F.blk"

RadioTower2F_Blocks:
	INCBIN "maps/gsc/RadioTower2F.blk"

RadioTower3F_Blocks:
	INCBIN "maps/gsc/RadioTower3F.blk"

RadioTower4F_Blocks:
	INCBIN "maps/gsc/RadioTower4F.blk"

RadioTower5F_Blocks:
	INCBIN "maps/gsc/RadioTower5F.blk"

NewBarkTown_Blocks:
	INCBIN "maps/gsc/NewBarkTown.blk"

CherrygroveCity_Blocks:
	INCBIN "maps/gsc/CherrygroveCity.blk"

VioletCity_Blocks:
	INCBIN "maps/gsc/VioletCity.blk"

AzaleaTown_Blocks:
	INCBIN "maps/gsc/AzaleaTown.blk"

CianwoodCity_Blocks:
	INCBIN "maps/gsc/CianwoodCity.blk"

GoldenrodCity_Blocks:
	INCBIN "maps/gsc/GoldenrodCity.blk"

OlivineCity_Blocks:
	INCBIN "maps/gsc/OlivineCity.blk"

EcruteakCity_Blocks:
	INCBIN "maps/gsc/EcruteakCity.blk"

MahoganyTown_Blocks:
	INCBIN "maps/gsc/MahoganyTown.blk"

LakeOfRage_Blocks:
	INCBIN "maps/gsc/LakeOfRage.blk"

BlackthornCity_Blocks:
	INCBIN "maps/gsc/BlackthornCity.blk"

SilverCaveOutside_Blocks:
	INCBIN "maps/gsc/SilverCaveOutside.blk"

Route6_Blocks:
	INCBIN "maps/gsc/Route6.blk"

Route7_Blocks:
	INCBIN "maps/gsc/Route7.blk"

Route16_Blocks:
	INCBIN "maps/gsc/Route16.blk"

Route18_Blocks:
	INCBIN "maps/gsc/Route18.blk"

GoldenrodUnderground_Blocks:
	INCBIN "maps/gsc/GoldenrodUnderground.blk"

GoldenrodUndergroundSwitchRoomEntrances_Blocks:
	INCBIN "maps/gsc/GoldenrodUndergroundSwitchRoomEntrances.blk"

GoldenrodDeptStoreB1F_Blocks:
	INCBIN "maps/gsc/GoldenrodDeptStoreB1F.blk"

GoldenrodUndergroundWarehouse_Blocks:
	INCBIN "maps/gsc/GoldenrodUndergroundWarehouse.blk"

TinTower1F_Blocks:
	INCBIN "maps/gsc/TinTower1F.blk"

TinTower2F_Blocks:
	INCBIN "maps/gsc/TinTower2F.blk"

TinTower3F_Blocks:
	INCBIN "maps/gsc/TinTower3F.blk"

TinTower4F_Blocks:
	INCBIN "maps/gsc/TinTower4F.blk"

TinTower5F_Blocks:
	INCBIN "maps/gsc/TinTower5F.blk"

TinTower6F_Blocks:
	INCBIN "maps/gsc/TinTower6F.blk"

TinTower7F_Blocks:
	INCBIN "maps/gsc/TinTower7F.blk"

TinTower8F_Blocks:
	INCBIN "maps/gsc/TinTower8F.blk"

TinTower9F_Blocks:
	INCBIN "maps/gsc/TinTower9F.blk"

TinTowerRoof_Blocks:
	INCBIN "maps/gsc/TinTowerRoof.blk"

BurnedTower1F_Blocks:
	INCBIN "maps/gsc/BurnedTower1F.blk"

BurnedTowerB1F_Blocks:
	INCBIN "maps/gsc/BurnedTowerB1F.blk"

MountMortar1FOutside_Blocks:
	INCBIN "maps/gsc/MountMortar1FOutside.blk"

MountMortar1FInside_Blocks:
	INCBIN "maps/gsc/MountMortar1FInside.blk"

MountMortar2FInside_Blocks:
	INCBIN "maps/gsc/MountMortar2FInside.blk"

MountMortarB1F_Blocks:
	INCBIN "maps/gsc/MountMortarB1F.blk"

IcePath1F_Blocks:
	INCBIN "maps/gsc/IcePath1F.blk"

IcePathB1F_Blocks:
	INCBIN "maps/gsc/IcePathB1F.blk"

IcePathB2FMahoganySide_Blocks:
	INCBIN "maps/gsc/IcePathB2FMahoganySide.blk"

IcePathB2FBlackthornSide_Blocks:
	INCBIN "maps/gsc/IcePathB2FBlackthornSide.blk"

IcePathB3F_Blocks:
	INCBIN "maps/gsc/IcePathB3F.blk"

WhirlIslandNW_Blocks:
	INCBIN "maps/gsc/WhirlIslandNW.blk"

WhirlIslandNE_Blocks:
	INCBIN "maps/gsc/WhirlIslandNE.blk"

WhirlIslandSW_Blocks:
	INCBIN "maps/gsc/WhirlIslandSW.blk"

WhirlIslandCave_Blocks:
	INCBIN "maps/gsc/WhirlIslandCave.blk"

WhirlIslandSE_Blocks:
	INCBIN "maps/gsc/WhirlIslandSE.blk"

WhirlIslandB1F_Blocks:
	INCBIN "maps/gsc/WhirlIslandB1F.blk"

WhirlIslandB2F_Blocks:
	INCBIN "maps/gsc/WhirlIslandB2F.blk"

WhirlIslandLugiaChamber_Blocks:
	INCBIN "maps/gsc/WhirlIslandLugiaChamber.blk"

SilverCaveRoom1_Blocks:
	INCBIN "maps/gsc/SilverCaveRoom1.blk"

SilverCaveRoom2_Blocks:
	INCBIN "maps/gsc/SilverCaveRoom2.blk"

SilverCaveRoom3_Blocks:
	INCBIN "maps/gsc/SilverCaveRoom3.blk"

MahoganyMart1F_Blocks:
MountMoonGiftShop_Blocks:
	INCBIN "maps/gsc/GiftShop.blk"

TeamRocketBaseB1F_Blocks:
	INCBIN "maps/gsc/TeamRocketBaseB1F.blk"

TeamRocketBaseB2F_Blocks:
	INCBIN "maps/gsc/TeamRocketBaseB2F.blk"

TeamRocketBaseB3F_Blocks:
	INCBIN "maps/gsc/TeamRocketBaseB3F.blk"

IndigoPlateauPokecenter1F_Blocks:
	INCBIN "maps/gsc/IndigoPlateauPokecenter1F.blk"

WillsRoom_Blocks:
	INCBIN "maps/gsc/WillsRoom.blk"

KogasRoom_Blocks:
	INCBIN "maps/gsc/KogasRoom.blk"

BrunosRoom_Blocks:
	INCBIN "maps/gsc/BrunosRoom.blk"

KarensRoom_Blocks:
	INCBIN "maps/gsc/KarensRoom.blk"

AzaleaGym_Blocks:
	INCBIN "maps/gsc/AzaleaGym.blk"

VioletGym_Blocks:
	INCBIN "maps/gsc/VioletGym.blk"

GoldenrodGym_Blocks:
	INCBIN "maps/gsc/GoldenrodGym.blk"

EcruteakGym_Blocks:
	INCBIN "maps/gsc/EcruteakGym.blk"

MahoganyGym_Blocks:
	INCBIN "maps/gsc/MahoganyGym.blk"

OlivineGym_Blocks:
	INCBIN "maps/gsc/OlivineGym.blk"

CianwoodGym_Blocks:
	INCBIN "maps/gsc/CianwoodGym.blk"

BlackthornGym1F_Blocks:
	INCBIN "maps/gsc/BlackthornGym1F.blk"

BlackthornGym2F_Blocks:
	INCBIN "maps/gsc/BlackthornGym2F.blk"

OlivineLighthouse1F_Blocks:
	INCBIN "maps/gsc/OlivineLighthouse1F.blk"

OlivineLighthouse2F_Blocks:
	INCBIN "maps/gsc/OlivineLighthouse2F.blk"

OlivineLighthouse3F_Blocks:
	INCBIN "maps/gsc/OlivineLighthouse3F.blk"

OlivineLighthouse4F_Blocks:
	INCBIN "maps/gsc/OlivineLighthouse4F.blk"

OlivineLighthouse5F_Blocks:
	INCBIN "maps/gsc/OlivineLighthouse5F.blk"

OlivineLighthouse6F_Blocks:
	INCBIN "maps/gsc/OlivineLighthouse6F.blk"


SECTION "Map Blocks 3", ROMX

SlowpokeWellB1F_Blocks:
	INCBIN "maps/gsc/SlowpokeWellB1F.blk"

SlowpokeWellB2F_Blocks:
	INCBIN "maps/gsc/SlowpokeWellB2F.blk"

IlexForest_Blocks:
	INCBIN "maps/gsc/IlexForest.blk"

DarkCaveVioletEntrance_Blocks:
	INCBIN "maps/gsc/DarkCaveVioletEntrance.blk"

DarkCaveBlackthornEntrance_Blocks:
	INCBIN "maps/gsc/DarkCaveBlackthornEntrance.blk"

RuinsOfAlphResearchCenter_Blocks:
	INCBIN "maps/gsc/RuinsOfAlphResearchCenter.blk"

GoldenrodBikeShop_Blocks:
	INCBIN "maps/gsc/GoldenrodBikeShop.blk"

DanceTheatre_Blocks:
	INCBIN "maps/gsc/DanceTheatre.blk"

EcruteakTinTowerEntrance_Blocks:
	INCBIN "maps/gsc/EcruteakTinTowerEntrance.blk"

GoldenrodGameCorner_Blocks:
	INCBIN "maps/gsc/GoldenrodGameCorner.blk"

Route35NationalParkGate_Blocks:
	INCBIN "maps/gsc/Route35NationalParkGate.blk"

Route36NationalParkGate_Blocks:
	INCBIN "maps/gsc/Route36NationalParkGate.blk"

FastShip1F_Blocks:
	INCBIN "maps/gsc/FastShip1F.blk"

FastShipB1F_Blocks:
	INCBIN "maps/gsc/FastShipB1F.blk"

FastShipCabins_NNW_NNE_NE_Blocks:
	INCBIN "maps/gsc/FastShipCabins_NNW_NNE_NE.blk"

FastShipCabins_SW_SSW_NW_Blocks:
	INCBIN "maps/gsc/FastShipCabins_SW_SSW_NW.blk"

FastShipCabins_SE_SSE_CaptainsCabin_Blocks:
	INCBIN "maps/gsc/FastShipCabins_SE_SSE_CaptainsCabin.blk"

OlivinePort_Blocks:
	INCBIN "maps/gsc/OlivinePort.blk"

VermilionPort_Blocks:
	INCBIN "maps/gsc/VermilionPort.blk"

OlivineCafe_Blocks:
SafariZoneMainOffice_Blocks:
	INCBIN "maps/gsc/OlivineCafe.blk"

PlayersHouse2F_Blocks:
	INCBIN "maps/gsc/PlayersHouse2F.blk"

SaffronMagnetTrainStation_Blocks:
	INCBIN "maps/gsc/SaffronMagnetTrainStation.blk"

CeruleanGym_Blocks:
	INCBIN "maps/gsc/CeruleanGym.blk"

VermilionGym_Blocks:
	INCBIN "maps/gsc/VermilionGym.blk"

SaffronGym_Blocks:
	INCBIN "maps/gsc/SaffronGym.blk"

PowerPlant_Blocks:
	INCBIN "maps/gsc/PowerPlant.blk"

PokemonFanClub_Blocks:
SafariZoneWardensHome_Blocks:
	INCBIN "maps/gsc/PokemonFanClub.blk"

FightingDojo_Blocks:
	INCBIN "maps/gsc/FightingDojo.blk"

SilphCo1F_Blocks:
	INCBIN "maps/gsc/SilphCo1F.blk"

ViridianGym_Blocks:
	INCBIN "maps/gsc/ViridianGym.blk"

TrainerHouse1F_Blocks:
	INCBIN "maps/gsc/TrainerHouse1F.blk"

TrainerHouseB1F_Blocks:
	INCBIN "maps/gsc/TrainerHouseB1F.blk"

RedsHouse1F_Blocks:
	INCBIN "maps/gsc/RedsHouse1F.blk"

RedsHouse2F_Blocks:
	INCBIN "maps/gsc/RedsHouse2F.blk"

OaksLab_Blocks:
	INCBIN "maps/gsc/OaksLab.blk"

MrFujisHouse_Blocks:
	INCBIN "maps/gsc/MrFujisHouse.blk"

LavRadioTower1F_Blocks:
	INCBIN "maps/gsc/LavRadioTower1F.blk"

SilverCaveItemRooms_Blocks:
	INCBIN "maps/gsc/SilverCaveItemRooms.blk"

DayCare_Blocks:
	INCBIN "maps/gsc/DayCare.blk"

SoulHouse_Blocks:
	INCBIN "maps/gsc/SoulHouse.blk"

PewterGym_Blocks:
	INCBIN "maps/gsc/PewterGym.blk"

CeladonGym_Blocks:
	INCBIN "maps/gsc/CeladonGym.blk"

CeladonCafe_Blocks:
	INCBIN "maps/gsc/CeladonCafe.blk"

RockTunnel1F_Blocks:
	INCBIN "maps/gsc/RockTunnel1F.blk"

RockTunnelB1F_Blocks:
	INCBIN "maps/gsc/RockTunnelB1F.blk"

DiglettsCave_Blocks:
	INCBIN "maps/gsc/DiglettsCave.blk"

MountMoon_Blocks:
	INCBIN "maps/gsc/MountMoon.blk"

SeafoamGym_Blocks:
	INCBIN "maps/gsc/SeafoamGym.blk"

MrPokemonsHouse_Blocks:
	INCBIN "maps/gsc/MrPokemonsHouse.blk"

VictoryRoadGate_Blocks:
	INCBIN "maps/gsc/VictoryRoadGate.blk"

OlivinePortPassage_Blocks:
VermilionPortPassage_Blocks:
	INCBIN "maps/gsc/PortPassage.blk"

FuchsiaGym_Blocks:
	INCBIN "maps/gsc/FuchsiaGym.blk"

SafariZoneBeta_Blocks:
	INCBIN "maps/gsc/SafariZoneBeta.blk"

UndergroundPath_Blocks:
	INCBIN "maps/gsc/UndergroundPath.blk"

Route39Barn_Blocks:
	INCBIN "maps/gsc/Route39Barn.blk"

VictoryRoad_Blocks:
	INCBIN "maps/gsc/VictoryRoad.blk"

Route23_Blocks:
	INCBIN "maps/gsc/Route23.blk"

LancesRoom_Blocks:
	INCBIN "maps/gsc/LancesRoom.blk"

HallOfFame_Blocks:
	INCBIN "maps/gsc/HallOfFame.blk"

CopycatsHouse1F_Blocks:
	INCBIN "maps/gsc/CopycatsHouse1F.blk"

CopycatsHouse2F_Blocks:
	INCBIN "maps/gsc/CopycatsHouse2F.blk"

GoldenrodFlowerShop_Blocks:
	INCBIN "maps/gsc/GoldenrodFlowerShop.blk"

MountMoonSquare_Blocks:
	INCBIN "maps/gsc/MountMoonSquare.blk"

WiseTriosRoom_Blocks:
	INCBIN "maps/gsc/WiseTriosRoom.blk"

DragonsDen1F_Blocks:
	INCBIN "maps/gsc/DragonsDen1F.blk"

DragonsDenB1F_Blocks:
	INCBIN "maps/gsc/DragonsDenB1F.blk"

TohjoFalls_Blocks:
	INCBIN "maps/gsc/TohjoFalls.blk"

RuinsOfAlphHoOhItemRoom_Blocks:
RuinsOfAlphKabutoItemRoom_Blocks:
RuinsOfAlphOmanyteItemRoom_Blocks:
RuinsOfAlphAerodactylItemRoom_Blocks:
	INCBIN "maps/gsc/RuinsOfAlphItemRoom.blk"

RuinsOfAlphHoOhWordRoom_Blocks:
	INCBIN "maps/gsc/RuinsOfAlphHoOhWordRoom.blk"

RuinsOfAlphKabutoWordRoom_Blocks:
	INCBIN "maps/gsc/RuinsOfAlphKabutoWordRoom.blk"

RuinsOfAlphOmanyteWordRoom_Blocks:
	INCBIN "maps/gsc/RuinsOfAlphOmanyteWordRoom.blk"

RuinsOfAlphAerodactylWordRoom_Blocks:
	INCBIN "maps/gsc/RuinsOfAlphAerodactylWordRoom.blk"

DragonShrine_Blocks:
	INCBIN "maps/gsc/DragonShrine.blk"

BattleTower1F_Blocks:
	INCBIN "maps/gsc/BattleTower1F.blk"

BattleTowerBattleRoom_Blocks:
	INCBIN "maps/gsc/BattleTowerBattleRoom.blk"

PokecomCenterAdminOfficeMobile_Blocks:
	INCBIN "maps/gsc/PokecomCenterAdminOfficeMobile.blk"

MobileTradeRoom_Blocks:
	INCBIN "maps/gsc/MobileTradeRoom.blk"

MobileBattleRoom_Blocks:
	INCBIN "maps/gsc/MobileBattleRoom.blk"

BattleTowerHallway_Blocks:
	INCBIN "maps/gsc/BattleTowerHallway.blk"

BattleTowerElevator_Blocks:
	INCBIN "maps/gsc/BattleTowerElevator.blk"

BattleTowerOutside_Blocks:
	INCBIN "maps/gsc/BattleTowerOutside.blk"

GoldenrodDeptStoreRoof_Blocks:
	INCBIN "maps/gsc/GoldenrodDeptStoreRoof.blk"
