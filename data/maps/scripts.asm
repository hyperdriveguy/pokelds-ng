SECTION "Map Scripts 1", ROMX

INCLUDE "maps/gsc/GoldenrodGym.asm"
INCLUDE "maps/gsc/GoldenrodBikeShop.asm"
INCLUDE "maps/gsc/GoldenrodHappinessRater.asm"
INCLUDE "maps/gsc/BillsFamilysHouse.asm"
INCLUDE "maps/gsc/GoldenrodMagnetTrainStation.asm"
INCLUDE "maps/gsc/GoldenrodFlowerShop.asm"
INCLUDE "maps/gsc/GoldenrodPPSpeechHouse.asm"
INCLUDE "maps/gsc/GoldenrodNameRater.asm"
INCLUDE "maps/gsc/GoldenrodDeptStore1F.asm"
INCLUDE "maps/gsc/GoldenrodDeptStore2F.asm"
INCLUDE "maps/gsc/GoldenrodDeptStore3F.asm"
INCLUDE "maps/gsc/GoldenrodDeptStore4F.asm"
INCLUDE "maps/gsc/GoldenrodDeptStore5F.asm"
INCLUDE "maps/gsc/GoldenrodDeptStore6F.asm"
INCLUDE "maps/gsc/GoldenrodDeptStoreElevator.asm"
INCLUDE "maps/gsc/GoldenrodDeptStoreRoof.asm"
INCLUDE "maps/gsc/GoldenrodGameCorner.asm"


SECTION "Map Scripts 2", ROMX

INCLUDE "maps/gsc/RuinsOfAlphOutside.asm"
INCLUDE "maps/gsc/RuinsOfAlphHoOhChamber.asm"
INCLUDE "maps/gsc/RuinsOfAlphKabutoChamber.asm"
INCLUDE "maps/gsc/RuinsOfAlphOmanyteChamber.asm"
INCLUDE "maps/gsc/RuinsOfAlphAerodactylChamber.asm"
INCLUDE "maps/gsc/RuinsOfAlphInnerChamber.asm"
INCLUDE "maps/gsc/RuinsOfAlphResearchCenter.asm"
INCLUDE "maps/gsc/RuinsOfAlphHoOhItemRoom.asm"
INCLUDE "maps/gsc/RuinsOfAlphKabutoItemRoom.asm"
INCLUDE "maps/gsc/RuinsOfAlphOmanyteItemRoom.asm"
INCLUDE "maps/gsc/RuinsOfAlphAerodactylItemRoom.asm"
INCLUDE "maps/gsc/RuinsOfAlphHoOhWordRoom.asm"
INCLUDE "maps/gsc/RuinsOfAlphKabutoWordRoom.asm"
INCLUDE "maps/gsc/RuinsOfAlphOmanyteWordRoom.asm"
INCLUDE "maps/gsc/RuinsOfAlphAerodactylWordRoom.asm"
INCLUDE "maps/gsc/UnionCave1F.asm"
INCLUDE "maps/gsc/UnionCaveB1F.asm"
INCLUDE "maps/gsc/UnionCaveB2F.asm"
INCLUDE "maps/gsc/SlowpokeWellB1F.asm"
INCLUDE "maps/gsc/SlowpokeWellB2F.asm"
INCLUDE "maps/gsc/OlivineLighthouse1F.asm"
INCLUDE "maps/gsc/OlivineLighthouse2F.asm"
INCLUDE "maps/gsc/OlivineLighthouse3F.asm"
INCLUDE "maps/gsc/OlivineLighthouse4F.asm"


SECTION "Map Scripts 3", ROMX

INCLUDE "maps/gsc/NationalPark.asm"
INCLUDE "maps/gsc/NationalParkBugContest.asm"
INCLUDE "maps/gsc/RadioTower1F.asm"
INCLUDE "maps/gsc/RadioTower2F.asm"
INCLUDE "maps/gsc/RadioTower3F.asm"
INCLUDE "maps/gsc/RadioTower4F.asm"


SECTION "Map Scripts 4", ROMX

INCLUDE "maps/gsc/RadioTower5F.asm"
INCLUDE "maps/gsc/OlivineLighthouse5F.asm"
INCLUDE "maps/gsc/OlivineLighthouse6F.asm"
INCLUDE "maps/gsc/GoldenrodPokecenter1F.asm"
INCLUDE "maps/gsc/PokecomCenterAdminOfficeMobile.asm"
INCLUDE "maps/gsc/IlexForestAzaleaGate.asm"
INCLUDE "maps/gsc/Route34IlexForestGate.asm"
INCLUDE "maps/gsc/DayCare.asm"


SECTION "Map Scripts 5", ROMX

INCLUDE "maps/gsc/Route11.asm"
INCLUDE "maps/gsc/VioletMart.asm"
INCLUDE "maps/gsc/VioletGym.asm"
INCLUDE "maps/gsc/EarlsPokemonAcademy.asm"
INCLUDE "maps/gsc/VioletNicknameSpeechHouse.asm"
INCLUDE "maps/gsc/VioletPokecenter1F.asm"
INCLUDE "maps/gsc/VioletKylesHouse.asm"
INCLUDE "maps/gsc/Route32RuinsOfAlphGate.asm"
INCLUDE "maps/gsc/Route32Pokecenter1F.asm"
INCLUDE "maps/gsc/Route35GoldenrodGate.asm"
INCLUDE "maps/gsc/Route35NationalParkGate.asm"
INCLUDE "maps/gsc/Route36RuinsOfAlphGate.asm"
INCLUDE "maps/gsc/Route36NationalParkGate.asm"


SECTION "Map Scripts 6", ROMX

INCLUDE "maps/gsc/Route8.asm"
INCLUDE "maps/gsc/MahoganyMart1F.asm"
INCLUDE "maps/gsc/TeamRocketBaseB1F.asm"
INCLUDE "maps/gsc/TeamRocketBaseB2F.asm"
INCLUDE "maps/gsc/TeamRocketBaseB3F.asm"
INCLUDE "maps/gsc/IlexForest.asm"


SECTION "Map Scripts 7", ROMX

INCLUDE "maps/gsc/LakeOfRage.asm"
INCLUDE "maps/gsc/CeladonDeptStore1F.asm"
INCLUDE "maps/gsc/CeladonDeptStore2F.asm"
INCLUDE "maps/gsc/CeladonDeptStore3F.asm"
INCLUDE "maps/gsc/CeladonDeptStore4F.asm"
INCLUDE "maps/gsc/CeladonDeptStore5F.asm"
INCLUDE "maps/gsc/CeladonDeptStore6F.asm"
INCLUDE "maps/gsc/CeladonDeptStoreElevator.asm"
INCLUDE "maps/gsc/CeladonMansion1F.asm"
INCLUDE "maps/gsc/CeladonMansion2F.asm"
INCLUDE "maps/gsc/CeladonMansion3F.asm"
INCLUDE "maps/gsc/CeladonMansionRoof.asm"
INCLUDE "maps/gsc/CeladonMansionRoofHouse.asm"
INCLUDE "maps/gsc/CeladonPokecenter1F.asm"
INCLUDE "maps/gsc/CeladonPokecenter2FBeta.asm"
INCLUDE "maps/gsc/CeladonGameCorner.asm"
INCLUDE "maps/gsc/CeladonGameCornerPrizeRoom.asm"
INCLUDE "maps/gsc/CeladonGym.asm"
INCLUDE "maps/gsc/CeladonCafe.asm"
INCLUDE "maps/gsc/Route16FuchsiaSpeechHouse.asm"
INCLUDE "maps/gsc/Route16Gate.asm"
INCLUDE "maps/gsc/Route7SaffronGate.asm"
INCLUDE "maps/gsc/Route17Route18Gate.asm"


SECTION "Map Scripts 8", ROMX

INCLUDE "maps/gsc/DiglettsCave.asm"
INCLUDE "maps/gsc/MountMoon.asm"
INCLUDE "maps/gsc/UndergroundPath.asm"
INCLUDE "maps/gsc/RockTunnel1F.asm"
INCLUDE "maps/gsc/RockTunnelB1F.asm"
INCLUDE "maps/gsc/SafariZoneFuchsiaGateBeta.asm"
INCLUDE "maps/gsc/SafariZoneBeta.asm"
INCLUDE "maps/gsc/VictoryRoad.asm"
INCLUDE "maps/gsc/OlivinePort.asm"
INCLUDE "maps/gsc/VermilionPort.asm"
INCLUDE "maps/gsc/FastShip1F.asm"
INCLUDE "maps/gsc/FastShipCabins_NNW_NNE_NE.asm"
INCLUDE "maps/gsc/FastShipCabins_SW_SSW_NW.asm"
INCLUDE "maps/gsc/FastShipCabins_SE_SSE_CaptainsCabin.asm"
INCLUDE "maps/gsc/FastShipB1F.asm"
INCLUDE "maps/gsc/OlivinePortPassage.asm"
INCLUDE "maps/gsc/VermilionPortPassage.asm"
INCLUDE "maps/gsc/MountMoonSquare.asm"
INCLUDE "maps/gsc/MountMoonGiftShop.asm"
INCLUDE "maps/gsc/TinTowerRoof.asm"


SECTION "Map Scripts 9", ROMX

INCLUDE "maps/gsc/Route34.asm"
INCLUDE "maps/gsc/ElmsLab.asm"
INCLUDE "maps/gsc/PlayersHouse1F.asm"
INCLUDE "maps/gsc/PlayersHouse2F.asm"
INCLUDE "maps/gsc/PlayersNeighborsHouse.asm"
INCLUDE "maps/gsc/ElmsHouse.asm"
INCLUDE "maps/gsc/Route26HealHouse.asm"
INCLUDE "maps/gsc/DayOfWeekSiblingsHouse.asm"
INCLUDE "maps/gsc/Route27SandstormHouse.asm"
INCLUDE "maps/gsc/Route29Route46Gate.asm"


SECTION "Map Scripts 10", ROMX

INCLUDE "maps/gsc/Route22.asm"
INCLUDE "maps/gsc/GoldenrodUnderground.asm"
INCLUDE "maps/gsc/GoldenrodUndergroundSwitchRoomEntrances.asm"
INCLUDE "maps/gsc/GoldenrodDeptStoreB1F.asm"
INCLUDE "maps/gsc/GoldenrodUndergroundWarehouse.asm"
INCLUDE "maps/gsc/MountMortar1FOutside.asm"
INCLUDE "maps/gsc/MountMortar1FInside.asm"
INCLUDE "maps/gsc/MountMortar2FInside.asm"
INCLUDE "maps/gsc/MountMortarB1F.asm"
INCLUDE "maps/gsc/IcePath1F.asm"
INCLUDE "maps/gsc/IcePathB1F.asm"
INCLUDE "maps/gsc/IcePathB2FMahoganySide.asm"
INCLUDE "maps/gsc/IcePathB2FBlackthornSide.asm"
INCLUDE "maps/gsc/IcePathB3F.asm"
INCLUDE "maps/gsc/LavenderPokecenter1F.asm"
INCLUDE "maps/gsc/LavenderPokecenter2FBeta.asm"
INCLUDE "maps/gsc/MrFujisHouse.asm"
INCLUDE "maps/gsc/LavenderSpeechHouse.asm"
INCLUDE "maps/gsc/LavenderNameRater.asm"
INCLUDE "maps/gsc/LavenderMart.asm"
INCLUDE "maps/gsc/SoulHouse.asm"
INCLUDE "maps/gsc/LavRadioTower1F.asm"
INCLUDE "maps/gsc/Route8SaffronGate.asm"
INCLUDE "maps/gsc/Route12SuperRodHouse.asm"


SECTION "Map Scripts 11", ROMX

INCLUDE "maps/gsc/EcruteakTinTowerEntrance.asm"
INCLUDE "maps/gsc/WiseTriosRoom.asm"
INCLUDE "maps/gsc/EcruteakPokecenter1F.asm"
INCLUDE "maps/gsc/EcruteakLugiaSpeechHouse.asm"
INCLUDE "maps/gsc/DanceTheatre.asm"
INCLUDE "maps/gsc/EcruteakMart.asm"
INCLUDE "maps/gsc/EcruteakGym.asm"
INCLUDE "maps/gsc/EcruteakItemfinderHouse.asm"
INCLUDE "maps/gsc/ViridianGym.asm"
INCLUDE "maps/gsc/ViridianNicknameSpeechHouse.asm"
INCLUDE "maps/gsc/TrainerHouse1F.asm"
INCLUDE "maps/gsc/TrainerHouseB1F.asm"
INCLUDE "maps/gsc/ViridianMart.asm"
INCLUDE "maps/gsc/ViridianPokecenter1F.asm"
INCLUDE "maps/gsc/ViridianPokecenter2FBeta.asm"
INCLUDE "maps/gsc/Route2NuggetHouse.asm"
INCLUDE "maps/gsc/Route2Gate.asm"
INCLUDE "maps/gsc/VictoryRoadGate.asm"


SECTION "Map Scripts 12", ROMX

INCLUDE "maps/gsc/OlivinePokecenter1F.asm"
INCLUDE "maps/gsc/OlivineGym.asm"
INCLUDE "maps/gsc/OlivineTimsHouse.asm"
INCLUDE "maps/gsc/OlivineHouseBeta.asm"
INCLUDE "maps/gsc/OlivinePunishmentSpeechHouse.asm"
INCLUDE "maps/gsc/OlivineGoodRodHouse.asm"
INCLUDE "maps/gsc/OlivineCafe.asm"
INCLUDE "maps/gsc/OlivineMart.asm"
INCLUDE "maps/gsc/Route38EcruteakGate.asm"
INCLUDE "maps/gsc/Route39Barn.asm"
INCLUDE "maps/gsc/Route39Farmhouse.asm"
INCLUDE "maps/gsc/ManiasHouse.asm"
INCLUDE "maps/gsc/CianwoodGym.asm"
INCLUDE "maps/gsc/CianwoodPokecenter1F.asm"
INCLUDE "maps/gsc/CianwoodPharmacy.asm"
INCLUDE "maps/gsc/CianwoodPhotoStudio.asm"
INCLUDE "maps/gsc/CianwoodLugiaSpeechHouse.asm"
INCLUDE "maps/gsc/PokeSeersHouse.asm"
INCLUDE "maps/gsc/BattleTower1F.asm"
INCLUDE "maps/gsc/BattleTowerBattleRoom.asm"
INCLUDE "maps/gsc/BattleTowerElevator.asm"
INCLUDE "maps/gsc/BattleTowerHallway.asm"
INCLUDE "maps/gsc/Route40BattleTowerGate.asm"
INCLUDE "maps/gsc/BattleTowerOutside.asm"


SECTION "Map Scripts 13", ROMX

INCLUDE "maps/gsc/IndigoPlateauPokecenter1F.asm"
INCLUDE "maps/gsc/WillsRoom.asm"
INCLUDE "maps/gsc/KogasRoom.asm"
INCLUDE "maps/gsc/BrunosRoom.asm"
INCLUDE "maps/gsc/KarensRoom.asm"
INCLUDE "maps/gsc/LancesRoom.asm"
INCLUDE "maps/gsc/HallOfFame.asm"


SECTION "Map Scripts 14", ROMX

INCLUDE "maps/gsc/CeruleanCity.asm"
INCLUDE "maps/gsc/SproutTower1F.asm"
INCLUDE "maps/gsc/SproutTower2F.asm"
INCLUDE "maps/gsc/SproutTower3F.asm"
INCLUDE "maps/gsc/TinTower1F.asm"
INCLUDE "maps/gsc/TinTower2F.asm"
INCLUDE "maps/gsc/TinTower3F.asm"
INCLUDE "maps/gsc/TinTower4F.asm"
INCLUDE "maps/gsc/TinTower5F.asm"
INCLUDE "maps/gsc/TinTower6F.asm"
INCLUDE "maps/gsc/TinTower7F.asm"
INCLUDE "maps/gsc/TinTower8F.asm"
INCLUDE "maps/gsc/TinTower9F.asm"
INCLUDE "maps/gsc/BurnedTower1F.asm"
INCLUDE "maps/gsc/BurnedTowerB1F.asm"


SECTION "Map Scripts 15", ROMX

INCLUDE "maps/gsc/CeruleanGymBadgeSpeechHouse.asm"
INCLUDE "maps/gsc/CeruleanPoliceStation.asm"
INCLUDE "maps/gsc/CeruleanTradeSpeechHouse.asm"
INCLUDE "maps/gsc/CeruleanPokecenter1F.asm"
INCLUDE "maps/gsc/CeruleanPokecenter2FBeta.asm"
INCLUDE "maps/gsc/CeruleanGym.asm"
INCLUDE "maps/gsc/CeruleanMart.asm"
INCLUDE "maps/gsc/Route10Pokecenter1F.asm"
INCLUDE "maps/gsc/Route10Pokecenter2FBeta.asm"
INCLUDE "maps/gsc/PowerPlant.asm"
INCLUDE "maps/gsc/BillsHouse.asm"
INCLUDE "maps/gsc/FightingDojo.asm"
INCLUDE "maps/gsc/SaffronGym.asm"
INCLUDE "maps/gsc/SaffronMart.asm"
INCLUDE "maps/gsc/SaffronPokecenter1F.asm"
INCLUDE "maps/gsc/SaffronPokecenter2FBeta.asm"
INCLUDE "maps/gsc/MrPsychicsHouse.asm"
INCLUDE "maps/gsc/SaffronMagnetTrainStation.asm"
INCLUDE "maps/gsc/SilphCo1F.asm"
INCLUDE "maps/gsc/CopycatsHouse1F.asm"
INCLUDE "maps/gsc/CopycatsHouse2F.asm"
INCLUDE "maps/gsc/Route5UndergroundPathEntrance.asm"
INCLUDE "maps/gsc/Route5SaffronGate.asm"
INCLUDE "maps/gsc/Route5CleanseTagHouse.asm"


SECTION "Map Scripts 16", ROMX

INCLUDE "maps/gsc/PewterCity.asm"
INCLUDE "maps/gsc/WhirlIslandNW.asm"
INCLUDE "maps/gsc/WhirlIslandNE.asm"
INCLUDE "maps/gsc/WhirlIslandSW.asm"
INCLUDE "maps/gsc/WhirlIslandCave.asm"
INCLUDE "maps/gsc/WhirlIslandSE.asm"
INCLUDE "maps/gsc/WhirlIslandB1F.asm"
INCLUDE "maps/gsc/WhirlIslandB2F.asm"
INCLUDE "maps/gsc/WhirlIslandLugiaChamber.asm"
INCLUDE "maps/gsc/SilverCaveRoom1.asm"
INCLUDE "maps/gsc/SilverCaveRoom2.asm"
INCLUDE "maps/gsc/SilverCaveRoom3.asm"
INCLUDE "maps/gsc/SilverCaveItemRooms.asm"
INCLUDE "maps/gsc/DarkCaveVioletEntrance.asm"
INCLUDE "maps/gsc/DarkCaveBlackthornEntrance.asm"
INCLUDE "maps/gsc/DragonsDen1F.asm"
INCLUDE "maps/gsc/DragonsDenB1F.asm"
INCLUDE "maps/gsc/DragonShrine.asm"
INCLUDE "maps/gsc/TohjoFalls.asm"
INCLUDE "maps/gsc/AzaleaPokecenter1F.asm"
INCLUDE "maps/gsc/CharcoalKiln.asm"
INCLUDE "maps/gsc/AzaleaMart.asm"
INCLUDE "maps/gsc/KurtsHouse.asm"
INCLUDE "maps/gsc/AzaleaGym.asm"


SECTION "Map Scripts 17", ROMX

INCLUDE "maps/gsc/MahoganyTown.asm"
INCLUDE "maps/gsc/Route32.asm"
INCLUDE "maps/gsc/VermilionFishingSpeechHouse.asm"
INCLUDE "maps/gsc/VermilionPokecenter1F.asm"
INCLUDE "maps/gsc/VermilionPokecenter2FBeta.asm"
INCLUDE "maps/gsc/PokemonFanClub.asm"
INCLUDE "maps/gsc/VermilionMagnetTrainSpeechHouse.asm"
INCLUDE "maps/gsc/VermilionMart.asm"
INCLUDE "maps/gsc/VermilionDiglettsCaveSpeechHouse.asm"
INCLUDE "maps/gsc/VermilionGym.asm"
INCLUDE "maps/gsc/Route6SaffronGate.asm"
INCLUDE "maps/gsc/Route6UndergroundPathEntrance.asm"
INCLUDE "maps/gsc/Pokecenter2F.asm"
INCLUDE "maps/gsc/TradeCenter.asm"
INCLUDE "maps/gsc/Colosseum.asm"
INCLUDE "maps/gsc/TimeCapsule.asm"
INCLUDE "maps/gsc/MobileTradeRoom.asm"
INCLUDE "maps/gsc/MobileBattleRoom.asm"


SECTION "Map Scripts 18", ROMX

INCLUDE "maps/gsc/Route36.asm"
INCLUDE "maps/gsc/FuchsiaCity.asm"
INCLUDE "maps/gsc/BlackthornGym1F.asm"
INCLUDE "maps/gsc/BlackthornGym2F.asm"
INCLUDE "maps/gsc/BlackthornDragonSpeechHouse.asm"
INCLUDE "maps/gsc/BlackthornEmysHouse.asm"
INCLUDE "maps/gsc/BlackthornMart.asm"
INCLUDE "maps/gsc/BlackthornPokecenter1F.asm"
INCLUDE "maps/gsc/MoveDeletersHouse.asm"
INCLUDE "maps/gsc/FuchsiaMart.asm"
INCLUDE "maps/gsc/SafariZoneMainOffice.asm"
INCLUDE "maps/gsc/FuchsiaGym.asm"
INCLUDE "maps/gsc/BillsBrothersHouse.asm"
INCLUDE "maps/gsc/FuchsiaPokecenter1F.asm"
INCLUDE "maps/gsc/FuchsiaPokecenter2FBeta.asm"
INCLUDE "maps/gsc/SafariZoneWardensHome.asm"
INCLUDE "maps/gsc/Route15FuchsiaGate.asm"
INCLUDE "maps/gsc/CherrygroveMart.asm"
INCLUDE "maps/gsc/CherrygrovePokecenter1F.asm"
INCLUDE "maps/gsc/CherrygroveGymSpeechHouse.asm"
INCLUDE "maps/gsc/GuideGentsHouse.asm"
INCLUDE "maps/gsc/CherrygroveEvolutionSpeechHouse.asm"
INCLUDE "maps/gsc/Route30BerryHouse.asm"
INCLUDE "maps/gsc/MrPokemonsHouse.asm"
INCLUDE "maps/gsc/Route31VioletGate.asm"


SECTION "Map Scripts 19", ROMX

INCLUDE "maps/gsc/AzaleaTown.asm"
INCLUDE "maps/gsc/GoldenrodCity.asm"
INCLUDE "maps/gsc/SaffronCity.asm"
INCLUDE "maps/gsc/MahoganyRedGyaradosSpeechHouse.asm"
INCLUDE "maps/gsc/MahoganyGym.asm"
INCLUDE "maps/gsc/MahoganyPokecenter1F.asm"
INCLUDE "maps/gsc/Route42EcruteakGate.asm"
INCLUDE "maps/gsc/LakeOfRageHiddenPowerHouse.asm"
INCLUDE "maps/gsc/LakeOfRageMagikarpHouse.asm"
INCLUDE "maps/gsc/Route43MahoganyGate.asm"
INCLUDE "maps/gsc/Route43Gate.asm"
INCLUDE "maps/gsc/RedsHouse1F.asm"
INCLUDE "maps/gsc/RedsHouse2F.asm"
INCLUDE "maps/gsc/BluesHouse.asm"
INCLUDE "maps/gsc/OaksLab.asm"


SECTION "Map Scripts 20", ROMX

INCLUDE "maps/gsc/CherrygroveCity.asm"
INCLUDE "maps/gsc/Route35.asm"
INCLUDE "maps/gsc/Route43.asm"
INCLUDE "maps/gsc/Route44.asm"
INCLUDE "maps/gsc/Route45.asm"
INCLUDE "maps/gsc/Route19.asm"
INCLUDE "maps/gsc/Route25.asm"


SECTION "Map Scripts 21", ROMX

INCLUDE "maps/gsc/CianwoodCity.asm"
INCLUDE "maps/gsc/Route27.asm"
INCLUDE "maps/gsc/Route29.asm"
INCLUDE "maps/gsc/Route30.asm"
INCLUDE "maps/gsc/Route38.asm"
INCLUDE "maps/gsc/Route13.asm"
INCLUDE "maps/gsc/PewterNidoranSpeechHouse.asm"
INCLUDE "maps/gsc/PewterGym.asm"
INCLUDE "maps/gsc/PewterMart.asm"
INCLUDE "maps/gsc/PewterPokecenter1F.asm"
INCLUDE "maps/gsc/PewterPokecenter2FBeta.asm"
INCLUDE "maps/gsc/PewterSnoozeSpeechHouse.asm"


SECTION "Map Scripts 22", ROMX

INCLUDE "maps/gsc/EcruteakCity.asm"
INCLUDE "maps/gsc/BlackthornCity.asm"
INCLUDE "maps/gsc/Route26.asm"
INCLUDE "maps/gsc/Route28.asm"
INCLUDE "maps/gsc/Route31.asm"
INCLUDE "maps/gsc/Route39.asm"
INCLUDE "maps/gsc/Route40.asm"
INCLUDE "maps/gsc/Route41.asm"
INCLUDE "maps/gsc/Route12.asm"


SECTION "Map Scripts 23", ROMX

INCLUDE "maps/gsc/NewBarkTown.asm"
INCLUDE "maps/gsc/VioletCity.asm"
INCLUDE "maps/gsc/OlivineCity.asm"
INCLUDE "maps/gsc/Route37.asm"
INCLUDE "maps/gsc/Route42.asm"
INCLUDE "maps/gsc/Route46.asm"
INCLUDE "maps/gsc/ViridianCity.asm"
INCLUDE "maps/gsc/CeladonCity.asm"
INCLUDE "maps/gsc/Route15.asm"
INCLUDE "maps/gsc/VermilionCity.asm"
INCLUDE "maps/gsc/Route9.asm"
INCLUDE "maps/gsc/CinnabarPokecenter1F.asm"
INCLUDE "maps/gsc/CinnabarPokecenter2FBeta.asm"
INCLUDE "maps/gsc/Route19FuchsiaGate.asm"
INCLUDE "maps/gsc/SeafoamGym.asm"


SECTION "Map Scripts 24", ROMX

INCLUDE "maps/gsc/Route33.asm"
INCLUDE "maps/gsc/Route2.asm"
INCLUDE "maps/gsc/Route1.asm"
INCLUDE "maps/gsc/PalletTown.asm"
INCLUDE "maps/gsc/Route21.asm"
INCLUDE "maps/gsc/CinnabarIsland.asm"
INCLUDE "maps/gsc/Route20.asm"
INCLUDE "maps/gsc/Route18.asm"
INCLUDE "maps/gsc/Route17.asm"
INCLUDE "maps/gsc/Route16.asm"
INCLUDE "maps/gsc/Route7.asm"
INCLUDE "maps/gsc/Route14.asm"
INCLUDE "maps/gsc/LavenderTown.asm"
INCLUDE "maps/gsc/Route6.asm"
INCLUDE "maps/gsc/Route5.asm"
INCLUDE "maps/gsc/Route24.asm"
INCLUDE "maps/gsc/Route3.asm"
INCLUDE "maps/gsc/Route4.asm"
INCLUDE "maps/gsc/Route10South.asm"
INCLUDE "maps/gsc/Route23.asm"
INCLUDE "maps/gsc/SilverCavePokecenter1F.asm"
INCLUDE "maps/gsc/Route28SteelWingHouse.asm"


SECTION "Map Scripts 25", ROMX

INCLUDE "maps/gsc/SilverCaveOutside.asm"
INCLUDE "maps/gsc/Route10North.asm"
